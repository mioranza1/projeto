# Passo-a-passo

```
sudo apt install unzip
curl -sSL get.k3s.io | sh
sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
sudo chown vagrant:vagrant .kube/config
sudo unlink /usr/local/bin/kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl
wget https://get.helm.sh/helm-v3.5.4-linux-amd64.tar.gz
tar -xzvf helm-v3.5.4-linux-amd64.tar.gz
sudo install -o root -g root -m 0755 linux-amd64/helm /usr/local/bin/helm
rm -rf kubectl helm-v3.5.4-linux-amd64.tar.gz linux-amd64/
```

# gitlab.yaml

```
apiVersion: v1
kind: Namespace
metadata:
  name: cicd
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab
  namespace: cicd
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: default
    namespace: cicd
```

# values.yml

```
gitlabUrl: https://gitlab.com/
runnerRegistrationToken: "bzNswq3SBPS_xY6Mop2B"
rbac:
  create: true
  clusterWideAccess: false
  #serviceAccountName: gitlab
runners:
  tags: "the-runner, the-runner-k8s"
  namespace: cicd
  pollTimeout: 600
  config: |
    [[runners]]
      [runners.kubernetes]
        image = "ubuntu:20.04"
        privileged = true
      [[runners.kubernetes.volumes.empty_dir]]
        name = "docker-certs"
        mount_path = "/certs/client"
        medium = "Memory"
  runtUntagged: true
securityContext:
  runAsUser: 100
  fsGroup: 65533
```

# helm install

```
helm repo add gitlab https://charts.gitlab.io
helm install --namespace cicd gitlab-runner -f values.yaml gitlab/gitlab-runner
helm uninstall gitlab-runner -n cicd
```

# instalar monitoramento

clonar repo https://github.com/cablespaghetti/k3s-monitoring

# projeto

clonar repo projeto
criar projeto novo no gitlab
setar remote do projeto clonado para o novo projeto
